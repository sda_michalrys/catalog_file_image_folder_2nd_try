package com.michalrys.catalog_f_i_f;

import com.michalrys.catalog_f_i_f.catalog.UserController;

public class Client {
    public static void main(String[] args) {
        UserController userController = new UserController();

        userController.add("file", "c:\\temp\\file.txt");
        userController.add("image", "c:\\temp\\file.jpg");
        userController.add("folder", "c:\\temp");

        UserController userController2 = new UserController(userController);

    }
}
