package com.michalrys.catalog_f_i_f.catalog;

import java.util.HashSet;
import java.util.Set;

class CatalogCRUD {
    private Set<CatalogedItem> storage;

    CatalogCRUD() {
        this.storage = new HashSet<>();
    }

    CatalogCRUD(CatalogCRUD existingCatalogCRUDToCopy) {
        storage = new HashSet<>();
        for(CatalogedItem item: existingCatalogCRUDToCopy.storage){
            storage.add(item);
        }
    }

    void add(File file) {
        storage.add(file);
    }

    void add(Image image) {
        storage.add(image);
    }

    void add(Folder folder) {
        storage.add(folder);
    }
}
