package com.michalrys.catalog_f_i_f.catalog;

public interface CatalogedItem {
    CatalogedItemType getType();

    Path getPath();
}
