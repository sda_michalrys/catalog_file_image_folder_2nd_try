package com.michalrys.catalog_f_i_f.catalog;

enum CatalogedItemType {
    FILE,
    IMAGE,
    FOLDER;
}
