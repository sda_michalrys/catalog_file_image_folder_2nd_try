package com.michalrys.catalog_f_i_f.catalog;

class File implements CatalogedItem {
    private final CatalogedItemType type;
    private final Path path;

    File(Path path) {
        type = CatalogedItemType.FILE;
        this.path = path;
    }

    @Override
    public CatalogedItemType getType() {
        return type;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        File file = (File) o;

        if (type != file.type) return false;
        return path != null ? path.equals(file.path) : file.path == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }
}
