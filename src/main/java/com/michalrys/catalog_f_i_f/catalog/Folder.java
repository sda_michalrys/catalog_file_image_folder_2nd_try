package com.michalrys.catalog_f_i_f.catalog;

class Folder implements CatalogedItem {
    private final Path path;
    private final CatalogedItemType type;

    Folder(Path path) {
        this.path = path;
        type = CatalogedItemType.FOLDER;
    }

    @Override
    public CatalogedItemType getType() {
        return type;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Folder folder = (Folder) o;

        if (path != null ? !path.equals(folder.path) : folder.path != null) return false;
        return type == folder.type;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
