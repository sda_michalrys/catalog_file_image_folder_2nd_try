package com.michalrys.catalog_f_i_f.catalog;

class Image implements CatalogedItem {
    private final Path path;
    private final CatalogedItemType type;

    Image(Path path) {
        this.path = path;
        type = CatalogedItemType.IMAGE;
    }

    @Override
    public CatalogedItemType getType() {
        return type;
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Image image = (Image) o;

        if (path != null ? !path.equals(image.path) : image.path != null) return false;
        return type == image.type;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
