package com.michalrys.catalog_f_i_f.catalog;

class Path {
    private final String path;

    public Path(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Path path1 = (Path) o;

        return path != null ? path.equals(path1.path) : path1.path == null;
    }

    @Override
    public int hashCode() {
        return path != null ? path.hashCode() : 0;
    }
}
