package com.michalrys.catalog_f_i_f.catalog;

public class UserController {
    private CatalogedItemTypeValidator catalogedItemTypeValidator;
    private PathValidator pathValidator;
    private CatalogCRUD catalogCRUD;

    public UserController() {
        catalogedItemTypeValidator = new CatalogedItemTypeValidator();
        pathValidator = new PathValidator();
        catalogCRUD = new CatalogCRUD();
    }

    public UserController(UserController existingUserControllerToCopy) {
        catalogedItemTypeValidator = new CatalogedItemTypeValidator();
        pathValidator = new PathValidator();
        catalogCRUD = new CatalogCRUD(existingUserControllerToCopy.catalogCRUD);
    }

    public void add(String whatToAdd, String UserInputPath) {
        if(!catalogedItemTypeValidator.isCorrect(whatToAdd)) {
            throw new WrongGivenTypeOfDataToAddException(whatToAdd);
        }

        if(!pathValidator.isCorrect(UserInputPath)) {
            throw new WrongGivenPathException(UserInputPath);
        }

        whatToAdd = whatToAdd.toUpperCase();
        CatalogedItemType catalogedItemType = CatalogedItemType.valueOf(whatToAdd);
        Path path = new Path(UserInputPath);

        if(catalogedItemType == CatalogedItemType.FILE) {
            catalogCRUD.add(new File(path));
        } else if (catalogedItemType == CatalogedItemType.IMAGE) {
            catalogCRUD.add(new Image(path));
        } else if (catalogedItemType == CatalogedItemType.FOLDER) {
            catalogCRUD.add(new Folder(path));
        }
    }
}
