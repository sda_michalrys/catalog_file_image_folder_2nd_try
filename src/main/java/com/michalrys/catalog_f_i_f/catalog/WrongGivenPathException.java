package com.michalrys.catalog_f_i_f.catalog;

class WrongGivenPathException extends RuntimeException {
    public WrongGivenPathException(String path) {
        super("Given path is wrong: " + path + ". There must my only \\ ora / separators.");
    }
}
