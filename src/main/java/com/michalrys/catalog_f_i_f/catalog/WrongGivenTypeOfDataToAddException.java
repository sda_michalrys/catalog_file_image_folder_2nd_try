package com.michalrys.catalog_f_i_f.catalog;

class WrongGivenTypeOfDataToAddException extends RuntimeException {
    public WrongGivenTypeOfDataToAddException(String whatToAdd) {
        super("Type of item to add is wrong : " + whatToAdd + ".");
    }
}
